<!DOCTYPE html>
<html lang="en">
<head>
    <title>Simple Time table</title>
	<meta charset="UTF-8">
	<meta name="description" content="WebUni Education Template">
	<meta name="keywords" content="webuni, education, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/font-awesome.min.css"/>
	<link rel="stylesheet" href="css/owl.carousel.css"/>
	<link rel="stylesheet" href="css/style.css"/>


	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>
    <!-- signup section -->
    <section class="signup-section spad">
        <div class="signup-bg set-bg" data-setbg="img/signup-bg.jpg"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="signup-warp">
                        <div class="section-title text-white text-left">
                            <h2>Sign In Now!</h2>
                            <p>You are Always welcome.</p>
                        </div>
                        <!-- signup form -->
                        <form class="signup-form make-new-input" method="post">
                            <input type="text" class="form-group matric" name="matricNumber" placeholder="Matric Number">
                            <input style="    height: 57px;
    width: 100%;
    padding: 0 30px;
    margin-bottom: 27px;
    float: left;
    border: none;
    font-size: 14px;
    font-weight: 500;
    background: #edf4f6;" type="password" class="form-group pass" name="password" placeholder="Your Password">
                            <button type="submit" class="site-btn login-dope-card">Search Couse</button>
                        </form>
                        <h5 ><a  style="color: #ffffff;" href="index.php">Not yet a member</a></h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- signup section end -->


	<!--====== Javascripts & Jquery ======-->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/mixitup.min.js"></script>
	<script src="js/circle-progress.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/main.js"></script>
    <script src="js/Login.js"></script>
</html>