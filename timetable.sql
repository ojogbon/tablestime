-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 10, 2020 at 09:28 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `timetable`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(222) NOT NULL,
  `password` varchar(222) NOT NULL,
  `date` timestamp NOT NULL,
  `status` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `date`, `status`) VALUES
(1, 'admin', 'admin', '2020-02-10 08:00:06', '0');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
CREATE TABLE IF NOT EXISTS `schedule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Department` varchar(222) NOT NULL,
  `Level` varchar(222) NOT NULL,
  `Coursecode` varchar(222) NOT NULL,
  `Lecturer` varchar(112) NOT NULL,
  `TimeRange` varchar(222) NOT NULL,
  `Days` varchar(222) NOT NULL,
  `date` timestamp NOT NULL,
  `status` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`id`, `Department`, `Level`, `Coursecode`, `Lecturer`, `TimeRange`, `Days`, `date`, `status`) VALUES
(1, 'csc', 'nd1', 'com112', 'Mrs olae', '9-10', 'Mon', '2020-02-10 09:18:10', '0'),
(2, 'computer', 'nd11', 'com113', 'Mrs Kunle', '1-2', 'Tues', '2020-02-10 09:24:53', '0'),
(3, 'computer', 'nd11', 'com113', 'Mr Seun', '2-3', 'Fir', '2020-02-10 09:25:29', '0'),
(4, 'computer', 'nd11', 'com213', 'Mrs Shine', '3-4', 'Thurs', '2020-02-10 09:25:55', '0'),
(5, 'csc', 'nd11', 'com313', 'Mrs shola', '11-12', 'Thurs', '2020-02-10 10:10:51', '0');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
CREATE TABLE IF NOT EXISTS `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(222) NOT NULL,
  `matricNumber` varchar(222) NOT NULL,
  `level` varchar(222) NOT NULL,
  `department` varchar(112) NOT NULL,
  `password` varchar(222) NOT NULL,
  `date` timestamp NOT NULL,
  `status` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `matricNumber`, `level`, `department`, `password`, `date`, `status`) VALUES
(1, 'sleep slim', '12/34/2002', 'nd1', 'csc', '7b21848ac9af35be0ddb2d6b9fc3851934db8420', '2020-02-10 05:42:33', '0'),
(2, 'food late', '12/34/67', 'nd11', 'computer', '1a9b9508b6003b68ddfe03a9c8cbc4bd4388339b', '2020-02-10 05:49:43', '0'),
(3, 'xame sike', '12/34/68', 'nd11', 'csc', '6934105ad50010b814c933314b1da6841431bc8b', '2020-02-10 10:07:19', '0');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
