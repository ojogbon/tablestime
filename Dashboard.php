<?php
    include 'includes/Schedule.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Simple Time table</title>
	<meta charset="UTF-8">
	<meta name="description" content="WebUni Education Template">
	<meta name="keywords" content="webuni, education, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/font-awesome.min.css"/>
	<link rel="stylesheet" href="css/owl.carousel.css"/>
	<link rel="stylesheet" href="css/style.css"/>


	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <style>
        .dash{
            text-align: center;
            font-family: cursive;
            border: 1px solid;
            padding: 5px;
            background-color: #28292d;
            color: #fff;
        }
    </style>
</head>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>


	<!-- Header section -->
	<header class="header-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3">
					<div class="site-logo">
                        <h2 style="color: #fff">SimpleTable</h2>
					</div>
					<div class="nav-switch">
						<i class="fa fa-bars"></i>
					</div>
				</div>

			</div>
		</div>
	</header>
	<!-- Header section end -->


	<!-- Page info -->
	<div style="height: 120px;" class="page-info-section set-bg" data-setbg="img/page-bg/3.jpg">

	</div>
	<!-- Page info end -->





	<!-- Page  -->
	<section class="blog-page spad pb-0">
		<div class="container">
			<div class="row">
				<div class="col-lg-9">
					<!-- blog post -->

;                    <?php
                        $myLevel = $_SESSION["level"];
                    $sql = "select * from schedule where Level = '".$myLevel."'";
                    $schedules = $schedule->getAllScheduleBySql($sql);
                    //print_r($schedules);
                    ?>
					<div class="blog-post">
                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper no-footer">
                            <table class="table display data-table text-nowrap dataTable no-footer"  role="grid">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc" rowspan="1" colspan="1" aria-label="SN" style="width: 134px;">Duration</th>
                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label=" Title: activate to sort column ascending" style="width: 372px;"> Department </th>
                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label=" Date: activate to sort column ascending" style="width: 296px;"> Level </th>
                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label=" Date: activate to sort column ascending" style="width: 296px;"> Coursecode </th>
                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label=" Date: activate to sort column ascending" style="width: 296px;"> Lecturer </th>
                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label=" Date: activate to sort column ascending" style="width: 296px;"> Days</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                    foreach ($schedules as $schedule => $value) {
                                        ?>
                                        <tr role="row" class="odd">
                                            <td class="sorting_1"><?php echo $schedules[$schedule]["TimeRange"];?></td>
                                            <td><?php echo $schedules[$schedule]["Department"];?></td>
                                            <td> <?php echo $schedules[$schedule]["Level"];?></td>
                                            <td class="sorting_1"><?php echo $schedules[$schedule]["Coursecode"];?> </td>
                                            <td><?php echo $schedules[$schedule]["Lecturer"];?></td>
                                            <td> <?php echo $schedules[$schedule]["Days"];?></td>


                                        </tr>
                                        <?php

                                    }
                                ?>


                                </tbody>
                            </table>
                            <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">

                            </div></div>
					</div>


				</div>
				<div class="col-lg-3 col-md-5 col-sm-9 sidebar">

					<div class="sb-widget-item">
						<h4 class="sb-w-title dash">Dashboard</h4>
						<ul>
							<li><a href="index.php">Home</a></li>
							<li><a href="#">Time table</a></li>
							<li><a href="logout.php">Logout</a></li>
						</ul>
					</div>

					<div class="sb-widget-item">
                        <h1>School  News</h1>
						<div class="add">
							<a href="#"><img src="img/add.jpg" alt=""></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Page end -->




	<!-- footer section -->
	<footer class="footer-section spad pb-0">

		<div class="footer-bottom">
			<div class="footer-warp">

				<div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                    and Developed by <a href="https://colorlib.com" target="_blank">CQT</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
			</div>
		</div>
	</footer> 
	<!-- footer section end -->


	<!--====== Javascripts & Jquery ======-->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/mixitup.min.js"></script>
	<script src="js/circle-progress.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/main.js"></script>
</body>
</html>